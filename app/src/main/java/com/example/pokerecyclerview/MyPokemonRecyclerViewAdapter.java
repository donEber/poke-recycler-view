package com.example.pokerecyclerview;

import androidx.recyclerview.widget.RecyclerView;

import android.media.Rating;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.pokerecyclerview.PokemonFragment.OnListFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyPokemonRecyclerViewAdapter extends RecyclerView.Adapter<MyPokemonRecyclerViewAdapter.ViewHolder> {

    private final List<Pokemon> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyPokemonRecyclerViewAdapter(List<Pokemon> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.textViewNombrePokemon.setText(holder.mItem.getNombre());
        holder.textViewTipo.setText(holder.mItem.getTipo());
        holder.ratingBarNivel.setRating(holder.mItem.getNivel());
        Picasso.get()
                .load(holder.mItem.getUrlFoto())
                .into(holder.imageViewFoto);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textViewNombrePokemon;
        public final TextView textViewTipo;
        public final ImageView imageViewFoto;
        public final RatingBar ratingBarNivel;

        public Pokemon mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            textViewNombrePokemon = view.findViewById(R.id.textViewNombre);
            textViewTipo = view.findViewById(R.id.textViewTipo);
            imageViewFoto = view.findViewById(R.id.imageViewFoto);
            ratingBarNivel = view.findViewById(R.id.ratingBarNivel);
        }
        /*
        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
        */
    }
}
