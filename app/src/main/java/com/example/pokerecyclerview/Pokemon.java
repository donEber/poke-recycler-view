package com.example.pokerecyclerview;

public class Pokemon {
    private String nombre;
    private String tipo;
    private int nivel;
    private String urlFoto;

    public Pokemon(String nombre, String tipo, int nivel, String urlFoto) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.nivel = nivel;
        this.urlFoto = urlFoto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}
